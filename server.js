//Der Body Parser übergibt die dAten aus dem HTML- Formular an die Server JS
//DEr Body-Parser wird im Terminal installiert mit dem befehlt : npm install body-parser --safe
var bodyParser = require('body-parser')



// Immer dann, wenn Daten dauerhaft gespeichert werden sollen, bedarf es einer Datenbank.
// Die meisten Datenbanken verwenden die Abfragesprache SQL.
// Wir wollen die Datenbank des Herstellers MySQL verwenden.
// MySQL muss in Node installier werden: nmp install mysql
// Installation von express in der Kosole: npm install express

// Das Modell mysql wird eingebunden

var mysql = require('mysql')

// Eine Verbindung zur Datenbank wird aufgebaut.


// Die Verbindungsdaten werden initialisiert.

var con = mysql.createConnection({
    host:"10.40.38.110",
    user:"schueler23",
    password:"schueler23",
    port: 3306, 
    database:"db23"
})


//Falls ein Fehler auftritt wird ein Fehler geworfen.

con.connect(function(err){

    if (err) throw err;

// Ansonsten wird folgendes ausgeführt:

    console.log("Erfolgreich mit der Datenbank verbunden!")


// Wenn die Verbindung zur Datenbank steht, dann wird eine Tabelle angelegt, sofern sie nicht existiert.


    con.query("CREATE TABLE IF NOT EXISTS taetigkeit(nummer INT AUTO_INCREMENT, von TIMESTAMP, bis DATETIME, beschreibung VARCHAR(50), PRIMARY KEY(nummer) );", function(err, result){
        if (err) throw err;

//Falls kein Fehler auftritt wird der Erfolg gemeldet

        console.log("Tabelle taetigkeit erfolgreich angelegt, bzw. schon vorhanden");
    });
})



    con.query("INSERT INTO taetigkeit (von,beschreibung) VALUES(now(),'Essen');", function(err, result){
       if (err) throw err;

        console.log("Taetigkeit 'Essen 'erfolgreich angelegt");
    }); 

    




// Die Server.js enthält die Logik für den Zugriff auf die Webseite
// Ein Framework soll die Programmierung vereinfachen. Es gibt viele verschiedene Frameworks.
// Das bereits mit npm installierte Express Framework wird eingebunden:
// Die folgende Zeile als natürlich sprachlicher Satz:
//"Das Framework Express wird eingebunden (require) und zugewiesen an (=) ein Objekt 
// namens express vom Typ Kostante (const)"

const express = require('express')

// Das app-Objekt wird von Express erzeugt. 
// Auf das app-Objekt werden im Folgenden die Express-Methoden aufgerufen.
// Das App-Objekt repräsentiert den Server mit all seinen Einstellungen.

const app = express()

// Unterhalb der Instanziierung der APP muss ejs eingebunden Werden.
//Der Sinn von ejs ist, das die Daten aus der DAtenbank an das Html Formular übergeben werden 


app.set('view engine', 'ejs')


app.use(bodyParser.urlencoded({extended: true}))

// Dem Server wird mitgeteilt, in welchem Ordner die statischen Inhalte liegen.
// Beispielsweise ist die Datei, die das Aussehen der App steuert (styles.css) dort abzulegen. 
// Statisch heißt, dass die Dateien zur Laufzeit nicht verändert werden.

//Unterhalb der Instanziierung der Express-App


app.use(express.static('public'))

// Wenn man von 'Server' spricht, dann kann zum einen ein Computer gemeint sein. 
// So ein Computer hat einen eindeutigen Namen und eine eindeutige IP-Adresse, unter der er vom Client-Computer erreicht werden kann.
// Ein Server in unserem Sinne ist ein Programm, das auf einem Computer ausgeführt wird und das einen bestimmten Dienst für Clients zur Verfügung stellt.
// Weil es viele verschiedene Dienste gibt, die zeitgleich auf einem Computer ausgeführt werden können, wird jedem Dienst eine eindeutige Portnummer zugewiesen. 
// Unser Node.js-Server soll z. B. auf Port 3000 laufen. Man sagt dann: 'Der Server lauscht auf Port 3000'. 
// Weil unser Server und der Client (Browser) auf dem selben Computer laufen, kann anstelle des eindeutigen Namens bzw. der eindeutigen IP-Adresse einfach
// 'localhost' in der Browseradresszeile eingegeben werden. Zusammen mit der Portnummer schreibt man dann in die Adresszeiel: 'localhost:3000'
// Zuvor muss der Server noch gestartet werden, indem im Terminal **node server.js** eingebenen wird.
// Das Terminal kann in VSC durch die Tastenkombination ***Strg+Ö*** geöffnet und geschlossen werden.
// Wenn alles klappt, wird der Erfolg im Terminal angezeigt:

app.listen(3000, function() {
    console.log('Der Server ist erfolgreich gestartet und lauscht auf Port 3000')
})

// Express kennt eine Methode namens get, die zwei Parameter entgegennimmt:

// 1. Der erste Parameter ist der Pfad, von dem die Anfrage kommt. '/' ist der Standardpfad.
//    Beispiel: Wenn der Anwender im Browser 'localhost:3000' eingibt, ist der Pfad '/' 
//              Wenn der Anwender im Browser 'localhost:3000/irgendwas' eingibt, ist der Pfad '/irgendwas' 
// 2. Der zweite Parameter ist die Callback-Funktion. Die sagt dem Server, wie er auf eine Anfrage vom Browser (englisch:Request) (req) mit einer Antwort (englisch: Response) (res) antworten soll.

app.get('/', function(req, res) {
    // Hier soll als Response auf den Request die index.html-Datei an den Browser zurückgegeben werden.
    res.sendFile(__dirname + '/index.html')
})

// Die app.post()-Methode leitet die Anfrage an den definierten Pfad weiter 
// mit der definierten Callback-Funktion.

//Die Funktion app.post() wird abgefeuert sobald der Button auf dem HTML-Formular geklickt wird.


app.post('/', function(req,res){
   
   //Dank des BodyParsers ist es nun möglich Werte aus dem HTML Formular an Javascript zu übergeben.
   // Die HTML Textbox mit dem NAME='TEXT' wird in JAVAscript abgefragt mit req.body.text 
    console.log("Button geklickt")
    console.log("Neue Tätigkeit namens " + req.body.text + " erfolgreich angelegt.");


    // Schreibt einen neuen Datensatz in die Datenbank!

    con.query("INSERT INTO taetigkeit (von,beschreibung) VALUES(now(),' " + req.body.text + "');", function(err, result){
    if (err) throw err;

     console.log("Taetigkeit ' " + req.body.text + "'erfolgreich angelegt");
    }); 

    //Alle Datensätze (Werden aus der Tabelle taetigkeit Abgefragt) Das * Steht für alle Spalten der Tablle



    con.query("SELECT * FROM taetigkeit", function(err, result){ 
    if (err) throw(err);
    //Die Variabel result beinhaltet das Rückgabeergebnis der SQL-Abfrage
    //"SELECT * FROM taetigkeit;"
    console.log(result);
    })

    //Die Funktion render bereitet die HtML Seite vor, in dem der Result an die HTML seite übergeben wird

    res.render('index.ejs',{            taetigkeiten:result                  })

});






